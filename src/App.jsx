import React from 'react';
import { hot } from 'react-hot-loader';
import SongsContainer from 'containers/SongsContainer';
import { Provider } from 'react-redux';
import configureStore from './store';

import './styles.scss';

const App = () => (
  <Provider store={configureStore()}>
    <SongsContainer />
  </Provider>
);

export default hot(module)(App);
