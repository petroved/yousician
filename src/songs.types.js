export const LOAD_SONGS = 'LOAD_SONGS';
export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE';
export const SET_SONGS_SEARCHED_IDS = 'SET_SONGS_SEARCHED_IDS';
export const SET_SONGS_FILTERED_IDS = 'SET_SONGS_FILTERED_IDS';
export const SET_FILTER_SONGS_LEVEL = 'SET_FILTER_SONGS_LEVEL';
export const SET_VISIBLE_SONGS = 'SET_VISIBLE_SONGS';
export const RESET_SONGS_FILTERS = 'RESET_SONGS_FILTERS';
export const CHANGE_SONG_RATING = 'CHANGE_SONG_RATING';

export const EMPTY = 'EMPTY';
export const PENDING = 'PENDING';
export const SUCCESS = 'SUCCESS';
export const ERROR = 'ERROR';
