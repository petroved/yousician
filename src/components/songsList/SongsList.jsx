import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import InfiniteScroll from 'react-infinite-scroller';
import SongsListItemContainer from 'containers/SongsListItemContainer';

const SongsList = ({ list, visibleSongs, showMoreSongs, resetFilters }) => {
  const items = [];
  const loader = (
    <div className="container" key={0}>
      <div className="loader">Loading ...</div>
    </div>
  );

  if (_.isArray(list) && list.length > 0) {
    _.map(list, (songId, i) => {
      if (i < visibleSongs) {
        items.push(<SongsListItemContainer key={songId} songId={songId} />);
      }
    });

    return (
      <InfiniteScroll
        className="songs-list"
        pageStart={0}
        loadMore={showMoreSongs}
        hasMore={list.length >= visibleSongs}
        loader={loader}
      >
        {items}
      </InfiniteScroll>
    );
  }

  return (
    <div className="align-container">
      <div className="songs-list__no-result">No results found</div>
      <div>
        <button className="btn" type="button" onClick={resetFilters}>
          Reset filters
        </button>
      </div>
    </div>
  );
};

SongsList.propTypes = {
  list: PropTypes.arrayOf(PropTypes.number).isRequired,
  visibleSongs: PropTypes.number.isRequired,
  showMoreSongs: PropTypes.func.isRequired,
  resetFilters: PropTypes.func.isRequired,
};

export default SongsList;
