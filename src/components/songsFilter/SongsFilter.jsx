import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Slider, Rail, Handles, Tracks, Ticks } from 'react-compound-slider';
import { Handle, Track, Tick } from './SongsFilterComponents';

import './songsFilter.styles.scss';

const SongsFilter = ({ domain, values, ticksCount, className, onChange }) => {
  let [minDomain, maxDomain] = domain;
  let disabled = false;

  if (!_.isFinite(minDomain) || !_.isFinite(maxDomain)) {
    disabled = true;
    minDomain = 0;
    maxDomain = 1;
  }

  if (
    _.isFinite(minDomain) &&
    _.isFinite(maxDomain) &&
    minDomain === maxDomain
  ) {
    disabled = true;
    minDomain -= 1;
  }

  return (
    <div className="container">
      <div className={className} title="Filter songs by level">
        <Slider
          className="slider"
          domain={[minDomain, maxDomain]}
          values={values}
          onChange={onChange}
          disabled={disabled}
          step={1}
        >
          <Rail>
            {({ getRailProps }) => (
              <div className="slider__rail" {...getRailProps()} />
            )}
          </Rail>
          <Handles>
            {({ handles, getHandleProps }) => (
              <div>
                {handles.map(handle => (
                  <Handle
                    key={handle.id}
                    handle={handle}
                    disabled={disabled}
                    getHandleProps={getHandleProps}
                  />
                ))}
              </div>
            )}
          </Handles>
          <Tracks left={false} right={false}>
            {({ tracks, getTrackProps }) => (
              <div className="slider-tracks">
                {tracks.map(({ id, source, target }) => (
                  <Track
                    key={id}
                    source={source}
                    target={target}
                    disabled={disabled}
                    getTrackProps={getTrackProps}
                  />
                ))}
              </div>
            )}
          </Tracks>
          <Ticks count={ticksCount}>
            {({ ticks }) => (
              <div className="slider-ticks">
                {ticks.map(tick => (
                  <Tick key={tick.id} tick={tick} count={ticks.length} />
                ))}
              </div>
            )}
          </Ticks>
        </Slider>
      </div>
    </div>
  );
};

SongsFilter.propTypes = {
  domain: PropTypes.arrayOf(PropTypes.number).isRequired,
  values: PropTypes.arrayOf(PropTypes.number).isRequired,
  ticksCount: PropTypes.number.isRequired,
  className: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default SongsFilter;
