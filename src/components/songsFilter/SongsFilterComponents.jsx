import React from 'react';
import PropTypes from 'prop-types';

const Handle = ({
  handle: { id, value, percent },
  disabled,
  getHandleProps,
}) => (
  <div
    style={{
      left: `${percent}%`,
      position: 'absolute',
      marginLeft: -12,
      marginTop: 28,
      zIndex: 2,
      width: 24,
      height: 24,
      border: 0,
      textAlign: 'center',
      cursor: 'pointer',
      borderRadius: '50%',
      backgroundColor: disabled ? '#555' : '#6fc13e',
      color: '#ccc',
    }}
    {...getHandleProps(id)}
  >
    <div style={{ fontSize: 18, marginTop: -20 }}>{value}</div>
  </div>
);

Handle.propTypes = {
  handle: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired,
  }).isRequired,
  disabled: PropTypes.bool.isRequired,
  getHandleProps: PropTypes.func.isRequired,
};

const Track = ({ source, target, disabled, getTrackProps }) => (
  <div
    style={{
      position: 'absolute',
      height: 10,
      zIndex: 1,
      marginTop: 35,
      backgroundColor: disabled ? '#555' : '#9fe96e',
      borderRadius: 5,
      cursor: 'pointer',
      left: `${source.percent}%`,
      width: `${target.percent - source.percent}%`,
    }}
    {...getTrackProps()}
  />
);

Track.propTypes = {
  source: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired,
  }).isRequired,
  target: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired,
  }).isRequired,
  disabled: PropTypes.bool.isRequired,
  getTrackProps: PropTypes.func.isRequired,
};

const Tick = ({ tick, count }) => (
  <div>
    <div
      style={{
        position: 'absolute',
        marginTop: 48,
        marginLeft: -0.5,
        width: 1,
        height: 8,
        backgroundColor: '#999',
        left: `${tick.percent}%`,
      }}
    />
    <div
      style={{
        position: 'absolute',
        marginTop: 56,
        fontSize: 14,
        textAlign: 'center',
        color: '#999',
        marginLeft: `${-(100 / count) / 2}%`,
        width: `${100 / count}%`,
        left: `${tick.percent}%`,
      }}
    >
      {tick.value}
    </div>
  </div>
);

Tick.propTypes = {
  tick: PropTypes.shape({
    id: PropTypes.string.isRequired,
    value: PropTypes.number.isRequired,
    percent: PropTypes.number.isRequired,
  }).isRequired,
  count: PropTypes.number.isRequired,
};

export { Handle, Track, Tick };
