import React from 'react';
import PropTypes from 'prop-types';
import RatingContainer from 'containers/RatingContainer';

import img from './fingerprint-white.png';

import './songsListItem.styles.scss';

const SongsListItem = ({ song, songId, dispatch, ...props }) => (
  <div className="songs-list-item" {...props}>
    <div className="container">
      <div className="songs-list-item__row">
        <div className="songs-list-item__image">
          <img alt={song.title} src={img} />
        </div>
        <div className="songs-list-item__level">{song.level}</div>
        <div className="songs-list-item__info">
          <div className="songs-list-item__title">{song.title}</div>
          <div className="songs-list-item__row songs-list-item__row--wrap">
            <RatingContainer
              className="songs-list-item__rating"
              songId={songId}
            />
            <div className="songs-list-item__artist">{song.artist}</div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

SongsListItem.propTypes = {
  dispatch: PropTypes.func,
  songId: PropTypes.number.isRequired,
  song: PropTypes.shape({
    _id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string.isRequired,
    released: PropTypes.string,
    rating: PropTypes.number.isRequired,
    level: PropTypes.number.isRequired,
  }).isRequired,
};

export default SongsListItem;
