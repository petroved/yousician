import React from 'react';
import PropTypes from 'prop-types';
import ReactRating from 'react-rating';

const SVGIcon = props => {
  const { className, href } = props;

  return (
    <svg className={className}>
      <use xlinkHref={href} />
    </svg>
  );
};

SVGIcon.propTypes = {
  className: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
};

const Rating = ({ rating, className, handleRatingChanged }) => (
  <>
    <svg
      aria-hidden="true"
      style={{
        position: 'absolute',
        width: 0,
        height: 0,
        overflow: 'hidden',
      }}
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xlinkHref="http://www.w3.org/1999/xlink"
    >
      <defs>
        <symbol id="icon-star-full" viewBox="0 0 32 32">
          <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z" />
        </symbol>
      </defs>
    </svg>
    <div className={className}>
      <ReactRating
        initialRating={rating}
        onChange={handleRatingChanged}
        fractions={10}
        emptySymbol={
          <SVGIcon href="#icon-star-full" className="icon icon-star-empty" />
        }
        fullSymbol={
          <SVGIcon href="#icon-star-full" className="icon icon-star-full" />
        }
      />
    </div>
  </>
);

Rating.propTypes = {
  rating: PropTypes.number.isRequired,
  className: PropTypes.string.isRequired,
  handleRatingChanged: PropTypes.func.isRequired,
};

export default Rating;
