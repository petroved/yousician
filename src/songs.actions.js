import {
  LOAD_SONGS,
  SET_SEARCH_VALUE,
  SET_SONGS_SEARCHED_IDS,
  SET_SONGS_FILTERED_IDS,
  SET_FILTER_SONGS_LEVEL,
  SET_VISIBLE_SONGS,
  RESET_SONGS_FILTERS,
  CHANGE_SONG_RATING,
} from './songs.types';

// Should use redux-thunk for real requests
import songs from './songs.json';

export function loadSongsList() {
  return {
    type: LOAD_SONGS,
    payload: songs,
  };
}

export function setSearchValue(value) {
  return {
    type: SET_SEARCH_VALUE,
    payload: value,
  };
}

export function setSongsSearchedIds(value) {
  return {
    type: SET_SONGS_SEARCHED_IDS,
    payload: value,
  };
}

export function setSongsFilteredIds(value) {
  return {
    type: SET_SONGS_FILTERED_IDS,
    payload: value,
  };
}

export function setFilterSongsLevel(value) {
  return {
    type: SET_FILTER_SONGS_LEVEL,
    payload: value,
  };
}

export function setVisibleSongs() {
  return {
    type: SET_VISIBLE_SONGS,
  };
}

export function resetSongsFilters() {
  return {
    type: RESET_SONGS_FILTERS,
  };
}

export function changeSongRating(value, songId) {
  return {
    type: CHANGE_SONG_RATING,
    payload: {
      rating: value,
      songId,
    },
  };
}
