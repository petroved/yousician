import { connect } from 'react-redux';
import SongsListItem from '../components/songsListItem/SongsListItem';

function mapStateToProps(state, { songId }) {
  return {
    song: state.songs.songsListMap[songId],
  };
}

export default connect(mapStateToProps)(SongsListItem);
