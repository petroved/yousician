import { connect } from 'react-redux';
import Rating from '../components/songsListItem/Rating';
import { changeSongRating } from '../songs.actions';

function mapStateToProps(state, { songId }) {
  return {
    rating: state.songs.songsListMap[songId].rating,
  };
}

function mapDispatchToProps(dispatch, { songId }) {
  return {
    handleRatingChanged(rating) {
      dispatch(changeSongRating(rating, songId));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Rating);
