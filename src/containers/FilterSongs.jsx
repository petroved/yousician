import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import SongsFilter from '../components/songsFilter/SongsFilter';
import { setFilterSongsLevel, setSongsFilteredIds } from '../songs.actions';

function mapStateToProps(state) {
  return {
    songsMap: state.songs.songsListMap,
    songsIds: state.songs.songsSearchedIds,
    filterSongsLevel: state.songs.filterSongsLevel,
    minMaxSongsLevel: state.songs.minMaxSongsLevel,
    ticksCount: state.songs.ticksCount,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setFilterLevel(filter) {
      dispatch(setFilterSongsLevel(filter));
    },
    setFilteredIds(ids) {
      dispatch(setSongsFilteredIds(ids));
    },
  };
}

class FilterSongs extends Component {
  constructor(props) {
    super(props);

    this.filterSongs = this.filterSongs.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { songsIds, filterSongsLevel } = this.props;

    if (!_.isEqual(songsIds, prevProps.songsIds)) {
      this.filterSongs();
    }

    if (!_.isEqual(filterSongsLevel, prevProps.filterSongsLevel)) {
      this.filterSongs();
    }
  }

  filterSongs() {
    const { songsIds, songsMap, filterSongsLevel, setFilteredIds } = this.props;
    const [minLevel, maxLevel] = filterSongsLevel;
    const resultArray = [];

    _.map(songsIds, songId => {
      if (
        songsMap[songId].level >= minLevel &&
        songsMap[songId].level <= maxLevel
      ) {
        resultArray.push(songId);
      }
    });

    setFilteredIds(resultArray);
  }

  handleFilterChange(values) {
    const { setFilterLevel } = this.props;

    setFilterLevel(values);
  }

  render() {
    const { minMaxSongsLevel, filterSongsLevel, ticksCount } = this.props;

    return (
      <SongsFilter
        className="songs-filter"
        domain={minMaxSongsLevel}
        values={filterSongsLevel}
        ticksCount={ticksCount}
        onChange={this.handleFilterChange}
      />
    );
  }
}

FilterSongs.propTypes = {
  songsMap: PropTypes.objectOf(
    PropTypes.shape({
      _id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      artist: PropTypes.string.isRequired,
      released: PropTypes.string,
      rating: PropTypes.number.isRequired,
      level: PropTypes.number.isRequired,
    }),
  ).isRequired,
  songsIds: PropTypes.arrayOf(PropTypes.number).isRequired,
  minMaxSongsLevel: PropTypes.arrayOf(PropTypes.number).isRequired,
  filterSongsLevel: PropTypes.arrayOf(PropTypes.number).isRequired,
  setFilterLevel: PropTypes.func.isRequired,
  setFilteredIds: PropTypes.func.isRequired,
  ticksCount: PropTypes.number.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(FilterSongs);
