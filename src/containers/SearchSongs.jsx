import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { setSearchValue, setSongsSearchedIds } from '../songs.actions';

function regexEscape(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function mapStateToProps(state) {
  return {
    songsMap: state.songs.songsListMap,
    searchString: state.songs.searchValue,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setSearchString(value) {
      dispatch(setSearchValue(value));
    },
    setSearchedIds(ids) {
      dispatch(setSongsSearchedIds(ids));
    },
  };
}

class SearchSongs extends Component {
  constructor(props) {
    super(props);

    this.searchSongs = this.searchSongs.bind(this);
    this.handleSearchStringChange = this.handleSearchStringChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { searchString } = this.props;

    if (!_.isEqual(searchString, prevProps.searchString)) {
      this.searchSongs();
    }
  }

  searchSongs() {
    const { searchString, songsMap, setSearchedIds } = this.props;

    const resultArray = [];
    const escapedString = regexEscape(searchString);

    _.mapValues(songsMap, item => {
      if (item.title.toLowerCase().indexOf(escapedString.toLowerCase()) === 0) {
        resultArray.push(item._id);
      }
    });

    _.mapValues(songsMap, item => {
      if (item.title.toLowerCase().indexOf(escapedString.toLowerCase()) > 0) {
        resultArray.push(item._id);
      }
    });

    _.mapValues(songsMap, item => {
      if (
        item.artist.toLowerCase().indexOf(escapedString.toLowerCase()) !== -1 &&
        !_.includes(resultArray, item._id)
      ) {
        resultArray.push(item._id);
      }
    });

    setSearchedIds(resultArray);
  }

  handleSearchStringChange(e) {
    const { setSearchString } = this.props;

    setSearchString(e.target.value);
  }

  render() {
    const { searchString } = this.props;

    return (
      <div className="songs-search">
        <div className="container">
          <input
            className="form-control"
            placeholder="Search songs"
            onChange={this.handleSearchStringChange}
            value={searchString}
          />
        </div>
      </div>
    );
  }
}

SearchSongs.propTypes = {
  songsMap: PropTypes.objectOf(
    PropTypes.shape({
      _id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      artist: PropTypes.string.isRequired,
      released: PropTypes.string,
      rating: PropTypes.number.isRequired,
      level: PropTypes.number.isRequired,
    }),
  ).isRequired,
  searchString: PropTypes.string.isRequired,
  setSearchString: PropTypes.func.isRequired,
  setSearchedIds: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchSongs);
