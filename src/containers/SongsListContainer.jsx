import { connect } from 'react-redux';
import SongsList from '../components/songsList/SongsList';
import { resetSongsFilters, setVisibleSongs } from '../songs.actions';

function mapStateToProps(state) {
  return {
    list: state.songs.songsFilteredIds,
    visibleSongs: state.songs.visibleSongs,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    showMoreSongs() {
      dispatch(setVisibleSongs());
    },
    resetFilters() {
      dispatch(resetSongsFilters());
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SongsList);
