import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SearchSongs from './SearchSongs';
import FilterSongs from './FilterSongs';
import SongsList from './SongsListContainer';
import { loadSongsList } from '../songs.actions';
import { SUCCESS, ERROR } from '../songs.types';

function mapStateToProps(state) {
  return {
    status: state.songs.status,
  };
}

function mapDispatch(dispatch) {
  return {
    loadSongs() {
      dispatch(loadSongsList());
    },
  };
}

class SongsContainer extends Component {
  componentDidMount() {
    const { loadSongs } = this.props;
    loadSongs();
  }

  render() {
    const { status } = this.props;

    if (status === SUCCESS) {
      return (
        <>
          <div className="songs-filters">
            <SearchSongs />
            <FilterSongs />
          </div>
          <SongsList />
        </>
      );
    }

    if (status === ERROR) {
      return <div className="align-container">Error while loading data</div>;
    }

    return <div className="align-container">No data</div>;
  }
}

SongsContainer.propTypes = {
  status: PropTypes.string.isRequired,
  loadSongs: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatch,
)(SongsContainer);
