import { combineReducers, createStore } from 'redux';
import songsListReducer from './songs.reducer';

export default function configureStore(initialState = {}) {
  const reducers = combineReducers({
    songs: songsListReducer,
  });

  return createStore(reducers, initialState);
}
