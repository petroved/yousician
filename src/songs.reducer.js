import _ from 'lodash';

import {
  LOAD_SONGS,
  SET_SEARCH_VALUE,
  SET_SONGS_SEARCHED_IDS,
  SET_SONGS_FILTERED_IDS,
  SET_FILTER_SONGS_LEVEL,
  SET_VISIBLE_SONGS,
  RESET_SONGS_FILTERS,
  CHANGE_SONG_RATING,
  EMPTY,
  SUCCESS,
} from './songs.types';

const INFINITE_SCROLL_COUNT = 8;
const FILTER_TICKS_RANGE = 5;

const initialState = {
  songsListMap: {},
  searchValue: '',
  visibleSongs: INFINITE_SCROLL_COUNT,
  songsSearchedIds: [],
  minMaxSongsLevel: [0, 10],
  filterSongsLevel: [0, 10],
  songsFilteredIds: [],
  ticksCount: 4,
  status: EMPTY,
};

export default function songsListReducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOAD_SONGS: {
      const songsMap = {};
      const listIds = [];
      const levelArray = [];

      if (_.isArray(payload) && payload.length > 0) {
        _.map(payload, (song, index) => {
          const extendedSong = song;
          const id = index + 1; // id starts from 1
          extendedSong._id = id;
          songsMap[id] = song;
          listIds.push(id);
          levelArray.push(song.level);
        });
      }

      const ticksNumber = Math.ceil(
        Math.max(...levelArray) / FILTER_TICKS_RANGE,
      );

      return {
        ...state,
        status: SUCCESS,
        songsSearchedIds: listIds,
        songsFilteredIds: listIds,
        songsListMap: songsMap,
        ticksCount: ticksNumber + 1,
        minMaxSongsLevel: [0, ticksNumber * FILTER_TICKS_RANGE],
        filterSongsLevel: [Math.min(...levelArray), Math.max(...levelArray)],
      };
    }
    case SET_SEARCH_VALUE: {
      return {
        ...state,
        searchValue: payload,
      };
    }
    case SET_SONGS_SEARCHED_IDS: {
      return {
        ...state,
        songsSearchedIds: payload,
      };
    }
    case SET_SONGS_FILTERED_IDS: {
      return {
        ...state,
        songsFilteredIds: payload,
      };
    }
    case SET_FILTER_SONGS_LEVEL: {
      return {
        ...state,
        filterSongsLevel: payload,
      };
    }
    case SET_VISIBLE_SONGS: {
      return {
        ...state,
        visibleSongs: state.visibleSongs + INFINITE_SCROLL_COUNT,
      };
    }
    case RESET_SONGS_FILTERS: {
      return {
        ...state,
        searchValue: '',
        filterSongsLevel: state.minMaxSongsLevel,
        visibleSongs: INFINITE_SCROLL_COUNT,
      };
    }
    case CHANGE_SONG_RATING: {
      const obj = state.songsListMap[payload.songId];
      obj.rating = payload.rating;

      return {
        ...state,
        songsListMap: {
          ...state.songsListMap,
          [payload.songId]: obj,
        },
      };
    }
    default:
      return state;
  }
}
