import React from 'react';
import { mount } from 'enzyme';

import App from '../src/App';
import SongsFilter from '../src/components/songsFilter/SongsFilter';
import SongsList from '../src/components/songsList/SongsList';
import SongsListItem from '../src/components/songsListItem/SongsListItem';

describe('<App />', () => {
  const wrap = mount(<App />);

  it('renders', () => {
    expect(wrap.find(App).exists()).toBe(true);
  });

  it('contains SongsFilter component', () => {
    expect(wrap.find(SongsFilter).exists()).toBe(true);
  });

  it('contains SongsList component', () => {
    expect(wrap.find(SongsList).exists()).toBe(true);
  });

  it('contains SongsListItem component', () => {
    expect(wrap.find(SongsListItem).exists()).toBe(true);
  });
});
